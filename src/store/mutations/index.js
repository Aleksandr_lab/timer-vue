export default {
  getModal(state, keyModal) {
    const allModal = { ...state.modal };
    Object.keys(allModal).map((key) => {
      if (key === keyModal) {
        allModal[key] = !allModal[key];
      } else {
        allModal[key] = false;
      }
      return key;
    });
    state.modal = allModal;
  },
};
