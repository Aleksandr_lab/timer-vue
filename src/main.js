import Vue from 'vue';
import VueYoutube from 'vue-youtube';
import App from './App.vue';
import store from './store';
import vuetify from './plugins/vuetify';
import './registerServiceWorker';

Vue.use(VueYoutube);

Vue.config.productionTip = false;

new Vue({
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
